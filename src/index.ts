/**
 * Represents values that can be one of two possible types.
 *
 * Instances of `Either` are either a `Left` or a `Right`.
 */
abstract class Either<A, B> {

  /**
   * Get either the value of `Left` or `Right`.
   */
  abstract get: A | B;

  /**
   * True when this is a `Left`.
   */
  abstract isLeft(): this is Left<A>;

  /**
   * True when this is a `Right`.
   */
  abstract isRight(): this is Right<B>;

  /**
   * Projects this as a `Left`, allowing operations on the left side.
   * If this is a `Right` the projection won't have any effect.
   */
  left(): LeftProjection<A, B> {
    return new LeftProjection(this);
  }

  /**
   * Swap this `Either` so that a `Left` becomes a `Right` or vice versa.
   */
  abstract swap(): Either<B, A>;

  /**
   * Apply `f` if this is a `Right` to make a new `Either` with the result.
   * @param f Function to apply if this is a `Right`.
   * @returns `Right` containing the application of `f` or `Left`.
   */
  abstract map<C>(f: (b: B) => C): Either<A, C>;

  /**
   * Apply `f` if this is a `Right`.
   * @param f Function to apply if this is a `Right`.
   * @returns The application of `f` or `Left`.
   */
  abstract flatMap<C>(f: (b: B) => Either<A, C>): Either<A, C>;

  /**
   * Apply `fa` if this is a `Left` or `fb` if this is a `Right`.
   * @param fa Function to apply if this is a `Left`.
   * @param fb Function to apply if this is a `Right`.
   * @returns The application of `fa` or `fb`.
   */
  abstract fold<C>(fa: (a: A) => C, fb: (b: B) => C): C;
}

class Left<A> extends Either<A, never> {
  constructor(readonly get: A) {
    super();
  }

  isLeft(): this is Left<A> {
    return true;
  }

  isRight(): this is Right<never> {
    return false;
  }

  swap<B>(): Either<B, A> {
    return new Right(this.get);
  }

  map(): Left<A> {
    return this;
  }

  flatMap(): Left<A> {
    return this;
  }

  fold<C>(fa: (a: A) => C): C {
    return fa(this.get);
  }
}

class LeftProjection<A, B> {
  constructor(readonly either: Either<A, B>) {}

  /**
   * Apply `f` if the projected `Either` is a `Left`.
   * @param f Function to apply if the projected `Either` is a `Left`.
   * @returns A new projection containing `Left(f(a))` or this.
   */
  map<C>(f: (a: A) => C): LeftProjection<C, B> {
    return this.either.isLeft()
      ? new LeftProjection(new Left(f(this.either.get)))
      : this as any;
  }

  /**
   * Apply `f` if the projected `Either` is a `Left`.
   * @param f Function to apply if the projected `Either` is a `Left`.
   * @returns A new projection containing `f(a)` or this.
   */
  flatMap<C>(f: (a: A) => Either<C, B>): LeftProjection<C, B> {
    return this.either.isLeft()
      ? new LeftProjection(f(this.either.get))
      : this as unknown as LeftProjection<C, B>;
  }
}

class Right<B> extends Either<never, B> {
  constructor(readonly get: B) {
    super();
  }

  isLeft(): this is Left<never> {
    return false;
  }

  isRight(): this is Right<B> {
    return true;
  }

  swap<A>(): Either<B, A> {
    return new Left(this.get);
  }

  map<C>(f: (b: B) => C): Right<C> {
    return new Right(f(this.get));
  }

  flatMap<A, C>(f: (b: B) => Either<A, C>): Either<A, C> {
    return f(this.get);
  }

  fold<C>(_: never, fb: (b: B) => C): C {
    return fb(this.get);
  }
}

/**
 * Create a `Left` with the given value.
 * @param a Underlying value.
 * @returns `Left(a)`.
 */
const left = <A>(a: A): Left<A> => new Left(a);

/**
 * Create a `Right` with the given value.
 * @param b Underlying value.
 * @returns `Right(b)`.
 */
const right = <B>(b: B): Right<B> => new Right(b);

export {
  Either,
  left as Left,
  right as Right,
};
