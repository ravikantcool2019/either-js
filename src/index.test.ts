import { Either, Left, Right } from '.';

describe('Either', () => {
  describe('Left', () => {
    describe('get', () => {
      it('returns the value', () => {
        expect(Left(1).get).toEqual(1);
        expect(Left("abc").get).toEqual("abc");
        const array: unknown[] = [];
        expect(Left(array).get).toEqual([]);
        expect(Left(array).get).toBe(array);
        const obj = Object.freeze({});
        expect(Left(obj).get).toBe(obj);
      });
    });

    describe('isLeft', () => {
      it('returns true', () => {
        expect(Left('Alice').isLeft()).toEqual(true);
      });
    });

    describe('isRight', () => {
      it('returns false', () => {
        expect(Left('Alice').isRight()).toEqual(false);
      });
    });

    describe('left', () => {
      it('projects to Left', () => {
        const left = Left('Alice');
        const projected = left.left();
        expect(projected.either).toBe(left);
        expect(projected.either).toEqual(Left('Alice'));
      });
    });

    describe('swap', () => {
      it('swaps to a Right', () => {
        const swapped = Left('Alice').swap();
        expect(swapped).toEqual(Right('Alice'));
        expect(swapped.isRight()).toEqual(true);
      });
    });

    describe('map', () => {
      it('returns this without invoking the function', () => {
        const f = jest.fn();
        const left: Either<string, never> = Left('Alice');
        const result = left.map(f);
        expect(result).toBe(left);
        expect(result).toEqual(Left('Alice'));
        expect(result.isLeft()).toEqual(true);
        expect(f).not.toHaveBeenCalled();
      });
    });

    describe('flatMap', () => {
      it('returns this without invoking the function', () => {
        const f = jest.fn();
        const left: Either<string, never> = Left('Alice');
        const result = left.flatMap(f);
        expect(result).toBe(left);
        expect(result).toEqual(Left('Alice'));
        expect(result.isLeft()).toEqual(true);
        expect(f).not.toHaveBeenCalled();
      });
    });

    describe('fold', () => {
      it('applies the given function to the value', () => {
        const fa = jest
          .fn<number, [string]>()
          .mockReturnValue(1);
        const fb = jest
          .fn<number, [string]>()
          .mockReturnValue(2);

        const left: Either<string, never> = Left('Alice');
        const result = left.fold<number>(fa, fb);
        expect(result).toEqual(1);
        expect(fa).toHaveBeenCalledTimes(1);
        expect(fa).toHaveBeenCalledWith('Alice');
        expect(fb).not.toHaveBeenCalled();
      });
    });
  });

  describe('Right', () => {
    describe('get', () => {
      it('returns the value', () => {
        expect(Right(1).get).toEqual(1);
        expect(Right("abc").get).toEqual("abc");
        const array: unknown[] = [];
        expect(Right(array).get).toEqual([]);
        expect(Right(array).get).toBe(array);
        const obj = Object.freeze({});
        expect(Right(obj).get).toBe(obj);
      });
    });

    describe('isLeft', () => {
      it('returns false', () => {
        expect(Right('Alice').isLeft()).toEqual(false);
      });
    });

    describe('isRight', () => {
      it('returns true', () => {
        expect(Right('Alice').isRight()).toEqual(true);
      });
    });

    describe('left', () => {
      it('projects to Left', () => {
        const right = Right('Alice');
        const projected = right.left();
        expect(projected.either).toBe(right);
        expect(projected.either).toEqual(Right('Alice'));
      });
    });

    describe('swap', () => {
      it('swaps to a Left', () => {
        const swapped = Right('Alice').swap();
        expect(swapped).toEqual(Left('Alice'));
        expect(swapped.isLeft()).toEqual(true);
      });
    });

    describe('map', () => {
      it('returns a Right containing the application of f', () => {
        const f = jest.fn<number, [string]>().mockReturnValue(123);
        const right: Either<never, string> = Right('Alice');
        const result = right.map(f);
        expect(result).toEqual(Right(123));
        expect(result.isRight()).toEqual(true);
        expect(f).toHaveBeenCalledTimes(1);
        expect(f).toHaveBeenCalledWith('Alice');
      });
    });

    describe('flatMap', () => {
      it('returns the application of f', () => {
        const f = jest
          .fn<Either<never, number>, [string]>()
          .mockReturnValue(Right(123));

        const right: Either<never, string> = Right('Alice');
        const result = right.flatMap(f);
        expect(result).toEqual(Right(123));
        expect(result.isRight()).toEqual(true);
        expect(f).toHaveBeenCalledTimes(1);
        expect(f).toHaveBeenCalledWith('Alice');
      });
    });

    describe('fold', () => {
      it('applies the given function to the value', () => {
        const fa = jest
          .fn<number, [string]>()
          .mockReturnValue(1);
        const fb = jest
          .fn<number, [string]>()
          .mockReturnValue(2);

        const right: Either<never, string> = Right('Alice');
        const result = right.fold<number>(fa, fb);
        expect(result).toEqual(2);
        expect(fb).toHaveBeenCalledTimes(1);
        expect(fb).toHaveBeenCalledWith('Alice');
        expect(fa).not.toHaveBeenCalled();
      });
    });
  });
});
