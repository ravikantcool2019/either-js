# @zcabjro/either

- [Summary](#summary)
- [Installation](#installation)
- [Methods](#methods)
- [Examples](#examples)
  - [get](#get)
  - [isLeft](#isleft)
  - [isRight](#isRight)
  - [map](#map)
  - [flatMap](#flatmap)
  - [fold](#fold)

## Summary

Represents values that can be one of two possible types.

Instances of `Either` are either a `Left` or a `Right`.

Inspired by Scala's `Either` type.

## Installation

```sh
npm install @zcabjro/either
```

## Methods

| Method                  | `Left`                                                             | `Right`                                                            |
|-------------------------|--------------------------------------------------------------------|--------------------------------------------------------------------|
| [get](#get)             | Returns the underlying value                                       | Returns the underlying value                                       |
| [isLeft](#isleft)       | Returns true                                                       | Returns false                                                      |
| [isRight](#isRight)     | Returns false                                                      | Returns true                                                       |
| [map](#map)             | noop                                                               | Applies a function to the underlying value                         |
| [flatMap](#flatmap)     | noop                                                               | Applies a function (that returns `Either`) to the underlying value |
| [fold](#fold)           | Applies a function to the underlying value and returns the result  | Applies a function to the underlying value and returns the result  |
| [left](#left)           | Projects to a `Left`                                               | Projects to a `Left`                                               |
| [swap](#swap)           | Returns `Right(a)`                                                 | Returns `Left(b)`                                                  |

## Examples

### `get`

Gets the underlying value from either `Left` or `Right`.

```js
Left("Alice").get // "Alice"
Right("Bob").get  // "Bob"
```

### `isLeft`

Returns true for `Left` and false for `Right`.

```js
Left("Alice").isLeft() // true
Right("Bob").isLeft()  // false
```

### `isRight`

Returns true for `Right` and false for `Left`.

```js
Left("Alice").isRight() // false
Right("Bob").isRight()  // true
```

### `map`

Applies the given function to a `Right`. Does nothing to a `Left`.

```js
Left("Alice").map(s => s.length)  // Left("Alice")
Right("Alice").map(s => s.length) // Right(5)
```

### `flatMap`

Returns the application of the function to a `Right`. Does nothing to a `Left`.

```js
Left("Alice").flatMap(() => Left(0))   // Left("Alice")
Left("Alice").flatMap(() => Right(1))  // Left("Alice")
Right("Alice").flatMap(() => Left(0))  // Left(0)
Right("Alice").flatMap(() => Right(1)) // Right(1)
```

### `fold`

Applies the appropriate function to `Left` or `Right`.

```js
Left("Alice").fold(() => 0, () => 1)   // 0
Right("Alice").fold(() => 0, () => 1)  // 1
```

### `left`

Projects to a `Left`, allowing operations on the left side.

The projection won't have any effect on a `Right`.

```js
Left("Alice").left().map(s => s.length).either;  // Left(5)
Right("Alice").left().map(s => s.length).either; // Right("Alice")
```

### `swap`

Swap so that a `Left` becomes a `Right` or vice versa.

```js
Left("Alice").swap();  // Right("Alice")
Right("Alice").swap(); // Left("Alice")
```
